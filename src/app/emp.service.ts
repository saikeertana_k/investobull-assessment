import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  constructor(private httpClient: HttpClient ) {
 
   }

   getAllValues(){
    return this.httpClient.get("https://intradayscreener.com/api/openhighlow/cash");
}
}
