import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrl: './table.component.css'
})
export class TableComponent implements OnInit {

  tables: any;

  ngOnInit(): void{ //executes at component initialization, when component is accessed
    this.service.getAllValues().subscribe((data : any)=>{
      this.tables = data;
      //console.log(data);
    })
  }

  constructor(private service : EmpService){

  }
}
